import { useState } from "react";
import "./App.css";

function App() {
  const [item, setItem] = useState([
    { id: 1, name: "Zero", count: 0 },
    { id: 2, name: "Zero", count: 0 },
    { id: 3, name: "Zero", count: 0 },
    { id: 4, name: "Zero", count: 0 },
  ]);
  const [selected, setSelected] = useState(0);

  function counterFunc() {
    let count = 0;
    item.map((entry) => {
      if (entry.count > 0) {
        count += 1;
      }
    });
    setSelected(count);
  }

  function increaseFunc(e) {
    setItem(
      item.filter((entry) => {
        if (entry.id == e.id) {
          entry.count += 1;
          entry.name = entry.count;
        }
        return entry;
      })
    );
    counterFunc();
  }
  function decreaseFunc(e) {
    setItem(
      item.filter((entry) => {
        if (entry.id == e.id) {
          if (entry.count > 0) {
            entry.count -= 1;
            entry.name = entry.count;
          }
          if (entry.name == 0) {
            entry.name = "Zero";
          }
        }
        return entry;
      })
    );
    counterFunc();
  }
  function deleteFunc(e) {
    setItem(
      item.filter((entry) => {
        if (entry.id == e.id) {
          entry.count = 0;
        }
        return entry.id != e.id;
      })
    );
    counterFunc();
  }

  function reset() {
    setItem(
      item.map((entry) => {
        entry.name = "Zero";
        entry.count = 0;
        return entry;
      })
    );
    counterFunc();
  }

  function hardReset() {
    if (item.length < 1) {
      setItem([
        { id: 1, name: "Zero", count: 0 },
        { id: 2, name: "Zero", count: 0 },
        { id: 3, name: "Zero", count: 0 },
        { id: 4, name: "Zero", count: 0 },
      ]);
    }
  }

  return (
    <>
      <div className="container">
        <div className="row1">
          <i className="fa-solid fa-cart-shopping"></i>
          <div className="display">{selected}</div>
          <div>Items</div>
        </div>
        <div className="row2">
          <button
            className="green"
            onClick={() => {
              reset();
            }}
          >
            <i className="fa-solid fa-repeat"></i>
          </button>
          <button
            className="blue"
            onClick={() => {
              hardReset();
            }}
          >
            <i class="fa-solid fa-recycle"></i>
          </button>
        </div>
        <div className="row3">
          {item.map((entry) => {
            return (
              <div className={entry.id} id="card">
                <div className="display2">{entry.name}</div>
                <button
                  className="grey"
                  onClick={() => {
                    increaseFunc(entry);
                  }}
                >
                  <i className="fa-solid fa-plus"></i>
                </button>
                <button
                  className="skyBlue"
                  onClick={() => {
                    decreaseFunc(entry);
                  }}
                >
                  <i class="fa-solid fa-minus"></i>
                </button>
                <button
                  className="red"
                  onClick={() => {
                    deleteFunc(entry);
                  }}
                >
                  <i className="fa-solid fa-trash"></i>
                </button>
              </div>
            );
          })}
        </div>
      </div>
    </>
  );
}

export default App;
